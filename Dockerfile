FROM nginx:alpine

COPY nginx.conf /etc/nginx/nginx.conf

WORKDIR /usr/share/nginx/html
RUN rm /usr/share/nginx/html/*
ADD ./dist/avantica-app/* /usr/share/nginx/html/
